export interface Company {
  id: string;
  companyName: string;
  companyNumber: string;
  companyIndustry: string;
}
