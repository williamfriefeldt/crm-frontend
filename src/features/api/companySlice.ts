import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { Company } from "../../entities/company";

export interface CreateCompanyData {
  companyName: string;
  companyNumber: string;
  companyIndustry: string;
}

export const companySlice = createApi({
  reducerPath: "companySlice",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://localhost:8080",
    mode: "cors",
  }),
  tagTypes: ["Company"],
  endpoints: (builder) => ({
    getCompanies: builder.query<Company[], void>({
      query: () => "/company",
    }),
    getCompany: builder.query<Company, string>({
      query: (companyId) => `/company/${companyId}`,
    }),
    createCompany: builder.mutation<Company, CreateCompanyData>({
      query: (data: CreateCompanyData) => ({
        url: "/company",
        method: "POST",
        body: data,
      }),
    }),
  }),
});

export const {
  useGetCompaniesQuery,
  useGetCompanyQuery,
  useCreateCompanyMutation,
} = companySlice;
