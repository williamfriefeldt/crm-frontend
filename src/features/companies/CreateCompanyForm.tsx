import { FormEvent, useState } from "react";
import { useCreateCompanyMutation } from "../api/companySlice";

type Props = {};

const CreateCompanyForm = (props: Props) => {
  const [companyName, setCompanyName] = useState("");
  const [companyNumber, setCompanyNumber] = useState("");
  const [companyIndustry, setCompanyIndustry] = useState("");

  const [addNewCompany, { isLoading }] = useCreateCompanyMutation();

  const canSave =
    [companyName, companyNumber, companyIndustry].every(Boolean) && !isLoading;

  const handleSubmit = async (event: FormEvent) => {
    event.preventDefault();

    if (canSave) {
      try {
        await addNewCompany({
          companyName,
          companyNumber,
          companyIndustry,
        }).unwrap();
        setCompanyName("");
        setCompanyNumber("");
        setCompanyIndustry("");
      } catch (err) {
        console.error("Failed to create the compnay: ", err);
      }
    } else {
      console.log("Can not save");
    }
  };

  return (
    <>
      <h2>Create new company</h2>
      <form onSubmit={handleSubmit} noValidate>
        <label htmlFor="name">Company name</label>
        <input
          id="name"
          type="text"
          value={companyName}
          onChange={(e) => setCompanyName(e.target.value)}
        ></input>
        <br />
        <label htmlFor="number">Company number</label>
        <input
          id="number"
          type="text"
          value={companyNumber}
          onChange={(e) => setCompanyNumber(e.target.value)}
        ></input>
        <br />
        <label htmlFor="industry">Company industry</label>
        <input
          id="industry"
          type="text"
          value={companyIndustry}
          onChange={(e) => setCompanyIndustry(e.target.value)}
        ></input>
        <br />
        <br />
        <button type="submit">Create</button>
      </form>
    </>
  );
};

export default CreateCompanyForm;
