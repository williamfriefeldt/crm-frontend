import { Link } from "react-router-dom";
import { useGetCompaniesQuery } from "../api/companySlice";

type Props = {};

const CompanyList = (props: Props) => {
  const { data: companies, isError, error } = useGetCompaniesQuery();

  if (companies) {
    return (
      <>
        {companies.map((company) => (
          <p key={company.id}>
            <Link to={`/company/${company.id}`}>
              {company.companyName} ({company.companyNumber}) -{" "}
              {company.companyIndustry}
            </Link>
          </p>
        ))}
      </>
    );
  } else if (isError) {
    return (
      <>
        <strong>Something went wrong...</strong>
      </>
    );
  } else {
    return <>Loading...</>;
  }
};

export default CompanyList;
