import { Link, Outlet } from "react-router-dom";
import "./layout.scss";

type Props = {};

const Layout = (props: Props) => {
  return (
    <div>
      <nav className="nav-bar">
        <div className="nav-list">
          <div className="nav-item">
            <Link className="nav-link" to="/">
              Home
            </Link>
          </div>
          <div className="nav-item">
            <Link className="nav-link" to="/companies">
              All companies
            </Link>
          </div>
        </div>
      </nav>

      <Outlet />
    </div>
  );
};

export default Layout;
