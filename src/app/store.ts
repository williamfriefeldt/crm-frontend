import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/dist/query";
import { companySlice } from "../features/api/companySlice";

export const store = configureStore({
  reducer: {
    [companySlice.reducerPath]: companySlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(companySlice.middleware),
});

setupListeners(store.dispatch);
