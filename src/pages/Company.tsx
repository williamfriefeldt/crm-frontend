import { useParams } from "react-router-dom";
import { useGetCompanyQuery } from "../features/api/companySlice";

type Props = {};

const Company = (props: Props) => {
  const { companyId } = useParams<{ companyId: string }>();
  const { data: company, isError, error } = useGetCompanyQuery(companyId!);

  if (company) {
    return (
      <>
        <h1>{company.companyName}</h1>
        <p>Company number: {company.companyNumber}</p>
        <p>Company industry: {company.companyIndustry}</p>
      </>
    );
  } else if (isError) {
    return <strong>Something went wrong...</strong>;
  } else {
    return <strong>Loading...</strong>;
  }
};

export default Company;
