import CompanyList from "../features/companies/CompanyList";
import CreateCompanyForm from "../features/companies/CreateCompanyForm";

type Props = {};

const Companies = (props: Props) => {
  return (
    <>
      <h1>Företag</h1>
      <CompanyList />
      <CreateCompanyForm />
    </>
  );
};

export default Companies;
