# CRM Frontend

A frontend, for [CRM backend](https://gitlab.com/sebastianhafstrom/crm), built with React and Redux.

## Getting started

### Frontend

1. Clone the project with `git clone [Projects ssh url]`
2. In the project, run `npm i` to install all packages
3. Start the dev server with `npm start`

### Backend

1. Clone [CRM backend](https://gitlab.com/sebastianhafstrom/crm) 
2. Intsall `docker` and `Java`
3. Start docker (for example by starting docker desktop)
4. Run ´docker-compose up´ to setup a database and to run the backend.

## Tests

Run `npm test`
